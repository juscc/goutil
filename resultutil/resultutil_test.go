package resultutil

import (
	"fmt"
	"testing"
)

func TestResult(t *testing.T) {
	var res, err = New("res.log")
	if err != nil {
		fmt.Println(err.Error())
	}
	fmt.Println(res.GetValue("hello"))
	res.SetValue("hello", "world")
	res.SetValue("hello", "nice")
	fmt.Println(res.GetValue("hello"))
	var m = make(map[string]string)
	m["hello"] = "golang"
	m["world"] = "python"
	m["python"] = "java"
	res.SetValues(m)
}
