package resultutil

import (
	"errors"
	"path/filepath"
	"strings"

	"gitee.com/juscc/goutil/fileutil"
	"gitee.com/juscc/goutil/sliceutil"
	"gitee.com/juscc/goutil/stringutil"
)

type result struct {
	logFilePath string
	resultMap   map[string]string
}

func New(logFilePath string) (*result, error) {
	if logFilePath == "" {
		return nil, errors.New("resultutil.logFilePath can not be empty")
	}
	if filepath.Ext(logFilePath) == "" {
		logFilePath = filepath.Join(logFilePath, "log.txt")
	}
	if err := validPath(logFilePath); err != nil {
		return nil, err
	} else {
		var res = &result{logFilePath: logFilePath}
		res.initFromLocalFile()
		return res, nil
	}
}

func (r *result) initFromLocalFile() error {
	if !fileutil.IsExists(r.logFilePath) {
		r.resultMap = make(map[string]string)
		return nil
	}
	var m, err = r.getDicFromLocal()
	r.resultMap = m
	if err != nil {
		return err
	}
	return nil
}

func (r *result) GetValue(key string) string {
	return r.resultMap[key]
}

func (r *result) getDicFromLocal() (map[string]string, error) {
	lines, err := fileutil.GetFileLines(r.logFilePath)
	if err != nil {
		return nil, err
	}
	var data = make(map[string]string)
	for _, lineCountent := range lines {
		var index = strings.Index(lineCountent, ":")
		if index <= 0 {
			continue
		}
		var key = strings.TrimSpace(lineCountent[0:index])
		var value = strings.TrimSpace(lineCountent[index+1:])
		if key != "" {
			data[key] = value
		}
	}
	return data, nil
}

func (r *result) SetValue(key, value string) error {
	r.resultMap[key] = value
	lines, err := fileutil.GetFileLines(r.logFilePath)
	if err != nil {
		return err
	}
	if len(lines) > 0 && strings.TrimSpace(lines[0]) == "" {
		lines = lines[1:]
	}
	var isExists = false
	for i, lineCountent := range lines {
		var index = strings.Index(lineCountent, ":")
		if index <= 0 {
			continue
		}
		var tkey = strings.TrimSpace(stringutil.SubString(lineCountent, 0, index))
		if key == tkey {
			lines[i] = key + ":" + value
			isExists = true
			break
		}
	}
	if !isExists {
		lines = append(lines, key+":"+value)
	}
	return fileutil.WriteTextFile(r.logFilePath, strings.Join(lines, "\r\n"))
}

func (r *result) SetValues(values map[string]string) error {
	lines, err := fileutil.GetFileLines(r.logFilePath)
	if err != nil {
		return err
	}
	if len(lines) > 0 && strings.TrimSpace(lines[0]) == "" {
		lines = lines[1:]
	}
	for key, value := range values {
		if _, isOk := r.resultMap[key]; isOk {
			var lineIndex = sliceutil.IndexOfFunc(lines, func(item *string) bool {
				return strings.HasPrefix(*item, key+":")
			})
			if lineIndex != -1 {
				lines[lineIndex] = key + ":" + value
			} else {
				lines = append(lines, key+":"+value)
			}
		} else {
			r.resultMap[key] = value
			lines = append(lines, key+":"+value)
		}
	}
	return fileutil.WriteTextFile(r.logFilePath, strings.Join(lines, "\r\n"))
}

// 如果路径不存在，则创建路径
func validPath(logFilePath string) error {
	var folder = filepath.Dir(logFilePath)
	if err := fileutil.CreateFolderIfNotExists(folder); err != nil {
		return err
	}
	if !fileutil.IsExists(logFilePath) {
		return fileutil.WriteFile(logFilePath, nil)
	}
	return nil
}
