package copyutil

import "github.com/jinzhu/copier"

// 复制对象属性
func Copy(to, from interface{}) {
	copier.Copy(to, from)
}
