package soundutil

import (
	"fmt"

	"github.com/go-ole/go-ole"
	"github.com/go-ole/go-ole/oleutil"
)

// 文档 https://blog.csdn.net/HuangZhang_123/article/details/126485195

type soundPlayer struct {
	voice *ole.IDispatch
}

func NewSoundPlayer() *soundPlayer {
	return &soundPlayer{}
}

// 初始化
func (s *soundPlayer) Init() {
	if err := ole.CoInitialize(0); err != nil {
		fmt.Println(err.Error())
	}
	unknown, err := oleutil.CreateObject("SAPI.SpVoice")
	if err != nil {
		fmt.Println(err.Error())
	}
	_voice, err := unknown.QueryInterface(ole.IID_IDispatch)
	if err != nil {
		fmt.Println(err.Error())
	}
	oleutil.PutProperty(_voice, "Volume", 300) // 设置音量
	s.voice = _voice
}

// 播放文本内容
//
//	content: 内容
//	rate: 语速(0为正常语速,正数为加速,负数为减速)
func (s *soundPlayer) Speak(content string, rate int) {
	if s.voice == nil {
		fmt.Println("请先调用Init()进行初始化")
		return
	}
	fmt.Println("speak: " + content)
	oleutil.PutProperty(s.voice, "Rate", rate) // 设置语速
	oleutil.CallMethod(s.voice, "Speak", content)

	// 停止说话
	//oleutil.CallMethod(voice, "Pause")
	// 恢复说话
	//oleutil.CallMethod(voice, "Resume")

	// 等待结束
	oleutil.CallMethod(s.voice, "WaitUntilDone", 1000000)
}

// 结束
func (s *soundPlayer) Close() {
	s.voice.Release()
	ole.CoUninitialize()
}
