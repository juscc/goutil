package configutil

import (
	"gitee.com/juscc/goutil/logutil"
	"github.com/fsnotify/fsnotify"
	"github.com/spf13/viper"
)

// 将配置文件参数转换为模型（支持多种文件类型）
//
//	filePath: 配置文件路径
//	modelReffrence: 模型的地址
func ToModel(filePath string, modelReffrence interface{}) error {
	v := viper.New()
	v.SetConfigFile(filePath)
	err := v.ReadInConfig()
	if err != nil {
		return err
	}
	v.WatchConfig()
	v.OnConfigChange(func(e fsnotify.Event) {
		if err := v.Unmarshal(modelReffrence); err != nil {
			logutil.Error("OnConfigChange error: " + err.Error())
		}
	})
	if err := v.Unmarshal(modelReffrence); err != nil {
		return err
	}
	return nil
}
