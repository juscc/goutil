package custypeutil

import (
	"database/sql/driver"
	"fmt"
	"time"
)

const dateTimeFormat = time.DateTime

// JsonDateTime format json time to 2006-01-02 15:04:05
type JsonDateTime struct {
	time.Time
}

func (t *JsonDateTime) UnmarshalJSON(data []byte) (err error) {
	if len(data) == 2 {
		*t = JsonDateTime{Time: time.Time{}}
		return
	}
	loc, err := time.LoadLocation("Asia/Shanghai")
	if err != nil {
		loc = time.FixedZone("CST", 8*3600) //替换上海时区方式
	}
	now, err := time.ParseInLocation(`"`+dateTimeFormat+`"`, string(data), loc)
	*t = JsonDateTime{Time: now}
	return
}

// MarshalJSON on JsonDateTime format Time field with Y-m-d H:i:s
func (t JsonDateTime) MarshalJSON() ([]byte, error) {
	if t.Time.IsZero() {
		return []byte("null"), nil
	}
	formatted := fmt.Sprintf("\"%s\"", t.Format(dateTimeFormat))
	return []byte(formatted), nil
}

// Value insert timestamp into mysql need this function.
func (t JsonDateTime) Value() (driver.Value, error) {
	var zeroTime time.Time
	if t.Time.UnixNano() == zeroTime.UnixNano() {
		return nil, nil
	}
	return t.Time, nil
}

// Scan value of time.Time
func (t *JsonDateTime) Scan(v interface{}) error {
	value, ok := v.(time.Time)
	if ok {
		*t = JsonDateTime{Time: value}
		return nil
	}
	return fmt.Errorf("can not convert %v to timestamp", v)
}
