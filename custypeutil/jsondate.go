package custypeutil

import (
	"database/sql/driver"
	"fmt"
	"time"
)

const dateFormat = time.DateOnly

// JsonDate format json time to 2006-01-02
type JsonDate struct {
	time.Time
}

func (t *JsonDate) UnmarshalJSON(data []byte) (err error) {
	if len(data) == 2 || len(data) == 4 { // "" 和 null 处理
		*t = JsonDate{Time: time.Time{}}
		return
	}
	loc, err := time.LoadLocation("Asia/Shanghai")
	if err != nil {
		loc = time.FixedZone("CST", 8*3600) //替换上海时区方式
	}
	now, err := time.ParseInLocation(`"`+dateFormat+`"`, string(data), loc)
	*t = JsonDate{Time: now}
	return
}

// MarshalJSON on JsonDate format Time field with Y-m-d H:i:s
func (t JsonDate) MarshalJSON() ([]byte, error) {
	if t.Time.IsZero() {
		return []byte(""), nil
	}
	formatted := fmt.Sprintf("\"%s\"", t.Format(dateFormat))
	return []byte(formatted), nil
}

// Value insert timestamp into mysql need this function.
func (t JsonDate) Value() (driver.Value, error) {
	var zeroTime time.Time
	if t.Time.UnixNano() == zeroTime.UnixNano() {
		return nil, nil
	}
	return t.Time, nil
}

// Scan value of time.Time
func (t *JsonDate) Scan(v interface{}) error {
	value, ok := v.(time.Time)
	if ok {
		*t = JsonDate{Time: value}
		return nil
	}
	return fmt.Errorf("can not convert %v to timestamp", v)
}
