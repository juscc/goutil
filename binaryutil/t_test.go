package binaryutil

import (
	"fmt"
	"testing"
)

type custype struct {
	A string
	B int
}

func TestToBinary(t *testing.T) {
	var c = custype{
		A: "hello world",
		B: 456,
	}
	var b, err = ToBinary(&c)
	fmt.Println(b, err)
	var d custype
	fmt.Println(ToModel(b, &d))
	fmt.Println(d)
}

func TestToBinaryFile(t *testing.T) {
	var c = custype{
		A: "hello world",
		B: 456,
	}
	fmt.Println(ToBinaryFile(&c, "hello.gob"))
}

func TestToModelFromFile(t *testing.T) {
	var c custype
	fmt.Println(ToModelFromFile("hello.gob", &c))
	fmt.Println(c)
}
