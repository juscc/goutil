package binaryutil

import (
	"bytes"
	"encoding/gob"
	"os"
)

// 将对象序列化为二进制数据
//
//	model: 需要序列化的对象地址
func ToBinary(model interface{}) ([]byte, error) {
	buffer := new(bytes.Buffer)
	encoder := gob.NewEncoder(buffer)
	err := encoder.Encode(model)
	return buffer.Bytes(), err
}

// 将对象序列化为二进制数据并保存到文件中
//
//	model: 需要序列化的对象地址
//	filename: 保存的文件路径
func ToBinaryFile(model interface{}, filename string) error {
	byts, err := ToBinary(model)
	if err != nil {
		return err
	}
	return os.WriteFile(filename, byts, 0600)
}

// 将二进制数据反序列化为对象
//
//	byts: 需要反序列化的二进制数据
//	model: 反序列化对象地址
func ToModel(byts []byte, model interface{}) error {
	buffer := bytes.NewBuffer(byts)
	dec := gob.NewDecoder(buffer)
	err := dec.Decode(model)
	return err
}

// 将文件中二进制数据反序列化为对象
//
//	filename: 文件路径
//	model: 反序列化对象地址
func ToModelFromFile(filename string, model interface{}) error {
	raw, err := os.ReadFile(filename)
	if err != nil {
		return err
	}
	return ToModel(raw, model)
}
