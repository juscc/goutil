package pageutil

import (
	"bytes"
	"io"
	"regexp"
	"strings"

	"gitee.com/juscc/goutil/logutil"
	"gitee.com/juscc/goutil/webutil"
)

func getCharset3(body string) string {
	var reg, _ = regexp.Compile(`<meta [^<>]*?charset=([^<>]*)?".*?>`)
	var matchs = reg.FindStringSubmatch(body)
	if len(matchs) == 2 {
		return strings.ReplaceAll(matchs[1], "\"", "")
	}
	return "utf-8"
}

// 是否需要转码
func isCharsetInvalid(charset string) bool {
	return strings.EqualFold(charset, "gb2312") || strings.EqualFold(charset, "gbk")
}

func getHtml(url string) (io.ReadCloser, error) {
	var res = webutil.GetWeb(url, webutil.GetBaseHeaders(), nil, "", 5)
	if res.Error != nil {
		logutil.Error(res.Error.Error())
		return nil, res.Error
	}
	return io.NopCloser(bytes.NewReader(res.RespBytes)), nil
}
