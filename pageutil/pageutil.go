package pageutil

import (
	"errors"
	"strings"

	"gitee.com/juscc/goutil/encodeutil"
	"gitee.com/juscc/goutil/sliceutil"
	"gitee.com/juscc/goutil/webutil"
	"github.com/PuerkitoBio/goquery"
	"golang.org/x/net/html"
)

type nodeCount struct {
	Node  *html.Node
	Count int
	Tag   string // 获取到该结点使用到的标签
}

// 1. 移除部分无效标签，如 ul, script, style
//
// 2. 查找 p 和 br 标签最密集的区域，找到该区域的父节点
//
// 3. 获取该节点的内部内容
func GetMainContent(url string) (title, content, charset string, err error) {
	if url == "" {
		err = errors.New("url is required")
		return
	}
	var resp = webutil.GetWeb(url, webutil.GetBaseHeaders(), nil, "", 5)
	if resp.Error != nil {
		err = resp.Error
		return
	}
	var htmlStr = resp.GetRespString()
	charset = getCharset3(htmlStr)
	var isCharsetInvalid = isCharsetInvalid(charset)
	if isCharsetInvalid {
		htmlStr = encodeutil.EncodeHtmlString(htmlStr)
	}
	htmlStr, _ = encodeutil.DecodeUnicodeString(htmlStr)

	htmlStr = strings.ReplaceAll(htmlStr, `&ldquo;`, "“")
	htmlStr = strings.ReplaceAll(htmlStr, `&rdquo;`, "”")
	htmlStr = strings.ReplaceAll(htmlStr, "<br />", "<br />\n")
	htmlStr = strings.ReplaceAll(htmlStr, "<br>", "<br />\n")

	// 使用goquery解析HTML文档
	doc, err := goquery.NewDocumentFromReader(strings.NewReader(htmlStr))
	if err != nil {
		return
	}
	doc.Find("ul").Remove()
	doc.Find("script").Remove()
	doc.Find("style").Remove()
	doc.Find("div[class*=foot]").Remove()
	doc.Find("div[class*=bottom]").Remove()
	doc.Find("div[class*=side]").Remove()

	var titleNodes = doc.Find("h1")
	if titleNodes.Length() > 0 {
		title = strings.TrimSpace(titleNodes.Text())
	} else {
		title = strings.TrimSpace(doc.Find("title").Text())
	}

	var contentTags = []string{"p", "br"}
	var resModel nodeCount // 之前有数据最多的节点
	for _, contentTag := range contentTags {
		var pNodes = doc.Find(contentTag)
		if pNodes.Length() == 0 {
			continue
		}
		var allContent string
		pNodes.Each(func(i int, s *goquery.Selection) {
			allContent += s.Text()
		})
		var repeatTimes = 1 // 向上查找父级的层级数，最多向上查找3层
	repeat:
		var nodeCountArry []nodeCount
		var nodeContents = make(map[*html.Node]string)
		pNodes.Each(func(i int, s *goquery.Selection) {
			var parentNode = s.Get(0)
			var content = s.Text()
			for i := 0; i < repeatTimes; i++ {
				parentNode = parentNode.Parent
			}
			nodeContents[parentNode] += content
			var first = sliceutil.First(nodeCountArry, func(item *nodeCount) bool { return item.Node == parentNode })
			if first == nil {
				nodeCountArry = append(nodeCountArry, nodeCount{Node: parentNode, Count: 1, Tag: contentTag})
			} else {
				first.Count++
			}
		})
		if len(nodeCountArry) > 0 {
			sliceutil.OrderBy(nodeCountArry, func(i, j int) bool { return nodeCountArry[i].Count > nodeCountArry[j].Count })
			if nodeCountArry[0].Count > resModel.Count {
				resModel = nodeCountArry[0]
			}
			// p标签按照内容长度判定是否为有效内容，br标签按照标签数量判定是否为有效内容
			if (contentTag == "p" && float32(len(nodeContents[resModel.Node]))/float32(len(allContent)) < 0.5) || (contentTag == "br" && nodeCountArry[0].Count < int(float32(pNodes.Length())*0.5)) {
				repeatTimes++
				if repeatTimes <= 3 {
					goto repeat
				}
			}
		}
	}
	if resModel.Node == nil {
		err = errors.New("not find valid data node")
		return
	}
	var sb strings.Builder
	var nodeDoc = goquery.NewDocumentFromNode(resModel.Node)
	if resModel.Tag == "br" {
		content = nodeDoc.Text()
	} else {
		nodeDoc.Children().Each(func(i int, st *goquery.Selection) {
			var text = st.Text()
			if strings.TrimSpace(text) == "" {
				return
			}
			sb.WriteString(text + "\n")
		})
		content = sb.String()
	}

	return
}
