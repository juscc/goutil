package pageutil

import (
	"errors"
	"strings"

	"gitee.com/juscc/goutil/encodeutil"
	"gitee.com/juscc/goutil/stringutil"
	"gitee.com/juscc/goutil/webutil"
	"github.com/PuerkitoBio/goquery"
	"golang.org/x/net/html"
)

type listItem struct {
	Title string
	Link  string
}

/*
1. 移除部分无用节点，如 script,style 等

2. 遍历所有链接，找到各个链接的最近的共同父节点

3. 查找列表数据所在的父节点（各子节点的文本的平均长度最长）

4. 从目标父节点中找出所有的数据链接

	a. 移除子节点中所有文本相同的链接
	b. 获取各子节点中所有的链接
*/
func GetListItems(url string) (items []listItem, nextPageUrl string, err error) {
	if url == "" {
		return nil, "", errors.New("url is empty")
	}
	var resp = webutil.GetWeb(url, webutil.GetBaseHeaders(), nil, "", 5)
	if resp.Error != nil {
		return nil, "", resp.Error
	}
	var htmlStr = resp.GetRespString()

	// 使用goquery解析HTML文档
	doc, err := goquery.NewDocumentFromReader(strings.NewReader(htmlStr))
	if err != nil {
		return nil, "", err
	}
	doc.Find("script").Remove()
	doc.Find("style").Remove()
	// doc.Find("nav").Remove()
	// doc.Find("div[class*=nav]").Remove()
	doc.Find("div[class*=foot]").Remove()
	doc.Find("div[class*=bottom]").Remove()
	doc.Find("div[class*=side]").Remove()
	doc.Find("div[class*=comment]").Remove()

	var charset = getCharset3(htmlStr)
	// 是否需要转码
	var isCharsetInvalid = isCharsetInvalid(charset)

	var aNodes = doc.Find("a")
	var parentNodesMap = make(map[*html.Node]int)
	aNodes.Each(func(i int, s *goquery.Selection) {
		if len(nextPageUrl) == 0 {
			var text = strings.TrimSpace(s.Text())
			if isCharsetInvalid {
				text = encodeutil.EncodeHtmlString(text)
			}
			if text == "下一页" || text == "下页" {
				nextPageUrl, _ = s.Attr("href")
			} else if strings.Contains(text, "下一页") || strings.Contains(text, "下页") {
				nextPageUrl, _ = s.Attr("href")
			}
		}
		var parentNode *html.Node
		for i := 0; i < 5; i++ {
			if parentNode == nil {
				parentNode = s.Get(0).Parent
			} else {
				parentNode = parentNode.Parent
			}
			if _, isOk := parentNodesMap[parentNode]; isOk {
				parentNodesMap[parentNode]++
				break // 找到第一个共同的父节点就停止再往上查找父节点了
			} else {
				parentNodesMap[parentNode] = 1
			}
		}
	})
	if len(parentNodesMap) == 0 {
		return nil, "", errors.New("not find valid data node")
	}
	var aimParentNode *html.Node     // 子节点平均文本最长的父节点
	var subNodeTextMaxtCount float64 // 子节点平均文本长度最大值
	for parentNode, count := range parentNodesMap {
		// 有多个子节点的才算，排除那些很靠上的父级节点
		if count <= 1 {
			continue
		}
		var subNodeCount int         // 子节点数量
		var allSubNodeTextLength int // 子节点文本长度之和
		var maxSubNodeTextLength int // 子节点中文本最长的长度
		goquery.NewDocumentFromNode(parentNode).Children().Each(func(i int, s *goquery.Selection) {
			var txt = strings.ReplaceAll(s.Text(), " ", "")
			var length = len(txt)
			allSubNodeTextLength += length
			subNodeCount++
			if length > maxSubNodeTextLength {
				maxSubNodeTextLength = length
			}
		})
		// 最长文本占总文本长度的百分比
		var maxLengthRate = float64(maxSubNodeTextLength) / float64(allSubNodeTextLength)
		// 子节点文本平均长度
		var avg = float64(allSubNodeTextLength) / float64(subNodeCount)
		if subNodeCount > 1 && maxLengthRate < 0.5 && avg > subNodeTextMaxtCount {
			subNodeTextMaxtCount = avg
			aimParentNode = parentNode
		}
	}

	// 从目标父节点中找出所有的数据链接
	var aimSelecter = goquery.NewDocumentFromNode(aimParentNode)
	// 子节点中所有链接
	var allLinks = aimSelecter.Find("a")
	// 链接文本与selecter字典
	var linkTextSelecterMap = make(map[string][]*goquery.Selection)
	allLinks.Each(func(i int, s *goquery.Selection) {
		var text = s.Text()
		linkTextSelecterMap[text] = append(linkTextSelecterMap[text], s)
	})
	// 移除子节点中所有文本相同的链接
	for _, selections := range linkTextSelecterMap {
		if len(selections) > 1 {
			for _, selection := range selections {
				selection.Remove()
			}
		}
	}

	// 过滤真实链接
	// 节点中只有一个链接的链接的href
	var oneLinkHref string
	aimSelecter.Children().Each(func(i int, s *goquery.Selection) {
		var as = s.Find("a")
		if as.Length() == 0 {
			return
		}
		if as.Length() == 1 {
			// 只有一个链接
			href, _ := as.Attr("href")
			oneLinkHref = href
			var text = as.Text()
			// 链接还有子节点
			if as.Children().Length() > 0 {
				var hs = as.Find("h")
				if hs.Length() > 0 {
					text = hs.Text()
				}
			}
			items = append(items, listItem{Title: text, Link: href})
		} else {
			// 有多个链接
			// 计算与只有单个链接的链接的相似度

			if oneLinkHref != "" {
				var aimSim float64
				var aimSelecter *goquery.Selection
				as.Each(func(i int, s *goquery.Selection) {
					var href, _ = s.Attr("href")
					_, sim := stringutil.Similarity(oneLinkHref, href)
					if sim > aimSim {
						aimSim = sim
						aimSelecter = s
					}
				})
				var href, _ = aimSelecter.Attr("href")
				items = append(items, listItem{Title: aimSelecter.Text(), Link: href})
			} else {
				// 取文本更长的那个

				var aimLength int
				var aimSelecter *goquery.Selection
				as.Each(func(i int, s *goquery.Selection) {
					length := len(s.Text())
					if length > aimLength {
						aimLength = length
						aimSelecter = s
					}
				})
				var href, _ = aimSelecter.Attr("href")
				items = append(items, listItem{Title: aimSelecter.Text(), Link: href})
			}
		}
	})

	for i := range items {
		if isCharsetInvalid {
			items[i].Title = encodeutil.EncodeHtmlString(items[i].Title)
		}
		items[i].Title = strings.TrimSpace(items[i].Title)
	}

	return items, nextPageUrl, nil
}
