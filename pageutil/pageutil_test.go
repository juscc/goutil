package pageutil

import (
	"fmt"
	"testing"
)

func TestPage(t *testing.T) {
	var address = "http://www.huaxiawen.com/rensheng/suigan/315.html"
	title, content, charset, err := GetMainContent(address)
	if err != nil {
		t.Error(err)
		return
	}
	fmt.Println(content)
	fmt.Println("标题:", title)
	fmt.Println("编码:", charset)
}

func TestList(t *testing.T) {
	var address = "http://www.huaxiawen.com/rensheng/suigan/"
	items, nextPageUrl, err := GetListItems(address)
	if err != nil {
		t.Error(err)
		return
	}
	fmt.Println(items)
	fmt.Println("下一页", nextPageUrl)
}
