package threadutil

import (
	"sync"
	"sync/atomic"
	"time"

	"gitee.com/juscc/goutil/errutil"
)

// 启用多线程完成任务
//
//	sources: 待处理的资源
//	threadCount: 最大线程数量
//	action: 具体任务
func Threading[T any](sources []T, threadCount int, action func(item *T)) {
	var sourceCount = len(sources)
	if len(sources) == 0 || threadCount == 0 || action == nil {
		return
	}
	if threadCount > sourceCount {
		threadCount = sourceCount
	}
	var group = &sync.WaitGroup{}
	var runningIndex atomic.Int32

	for i := 0; i < threadCount; i++ {
		group.Add(1)
		go func() {
			for {
				var index = int(runningIndex.Load())
				if index >= sourceCount {
					break
				}
				runningIndex.Add(1)
				action(&sources[index])
			}
			group.Done()
		}()
	}
	group.Wait()
}

// time.NewTicker
func NewTicker(f func(), t time.Duration) {
	defer errutil.CatchError()
	var tk = time.NewTicker(t)
	f()
	for range tk.C {
		func() {
			defer errutil.CatchError()
			f()
		}()
	}
}

// 每小时内间隔一定时间重复执行任务
//
//	timeInterval: 执行任务的时间间隔（分钟）
//	timeWait: 执行任务前的等待时间（分钟）
func TimeInterval(f func(), timeInterval, timeWait int) {
	for {
		if time.Now().Minute()%timeInterval == timeWait {
			func() {
				defer errutil.CatchError()
				f()
			}()
		}
		time.Sleep(time.Duration(60-time.Now().Second()) * time.Second)
	}
}

// 间隔一定时间重复执行任务，第一次任务会立即执行
func TimeInterval2(f func(), timeInterval time.Duration) {
	var lastTime time.Time
	for {
		if lastTime.Add(timeInterval).Before(time.Now()) {
			func() {
				defer errutil.CatchError()
				f()
				lastTime = time.Now()
			}()
		}
		time.Sleep(timeInterval)
	}
}

// 每天定时执行一次任务
//
//	hour: 定时任务执行的小时
//	minute: 定时任务执行的分钟
//	f: 定时任务执行的函数
func EveryDay(hour, minute int, f func()) {
	for {
		if time.Now().Hour() == hour && time.Now().Minute() == minute {
			func() {
				defer errutil.CatchError()
				f()
			}()
		}
		time.Sleep(time.Duration(60-time.Now().Second()) * time.Second)
	}
}
