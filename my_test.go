package main

import (
	"fmt"
	"testing"

	"gitee.com/juscc/goutil/timeutil"
)

func TestConsole(t *testing.T) {
	fmt.Println("Hello World")
}

func TestDate(t *testing.T) {
	fmt.Println(timeutil.GetTomorrowDate())
	fmt.Println(timeutil.GetYesterdayDate())
}
