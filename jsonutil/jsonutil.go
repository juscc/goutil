package jsonutil

import (
	"github.com/bytedance/sonic"
)

// 将模型转换为JSON字符串
func ToJson(obj interface{}) (string, error) {
	bytes, err := sonic.Marshal(obj)
	if err != nil {
		return "", err
	}
	str := string(bytes)
	return str, nil
}

// 将JSON字符串转换为模型
func ToModel(jsonString string, model interface{}) error {
	return sonic.Unmarshal([]byte(jsonString), &model)
}

// 是否是有效的JSON字符串
func IsJson(jsonString string) bool {
	return sonic.ValidString(jsonString)
}
