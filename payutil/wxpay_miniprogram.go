package payutil

import (
	"errors"
	"fmt"
	"path"
	"time"

	"gitee.com/juscc/goutil/encryptutil"
	"gitee.com/juscc/goutil/fileutil"
	"gitee.com/juscc/goutil/jsonutil"
	"gitee.com/juscc/goutil/logutil"
	"gitee.com/juscc/goutil/randutil"
	"gitee.com/juscc/goutil/timeutil"
	"gitee.com/juscc/goutil/webutil"
)

// https://pay.weixin.qq.com/wiki/doc/apiv3/index.shtml
// https://pay.weixin.qq.com/wiki/doc/apiv3/apis/chapter3_5_1.shtml

const (
	jsapi               = "https://api.mch.weixin.qq.com/v3/pay/transactions/jsapi"
	query1              = "https://api.mch.weixin.qq.com/v3/pay/transactions/id/"
	query2              = "https://api.mch.weixin.qq.com/v3/pay/transactions/out-trade-no/"
	certificates        = "https://api.mch.weixin.qq.com/v3/certificates"      // 获取商户当前可用的平台证书列表
	tradebill           = "https://api.mch.weixin.qq.com/v3/bill/tradebill"    // 申请交易单
	fundflowbill        = "https://api.mch.weixin.qq.com/v3/bill/fundflowbill" // 申请资金账单
	tradebillSaveFolder = "tradebill"
)

type wxminipay struct {
	appid                 string // 由微信生成的应用ID，全局唯一。示例值：wxd678efh567hg6787
	mchid                 string // 直连商户的商户号，由微信支付生成并下发。示例值：1230000109
	notify_url            string // 异步接收微信支付结果通知的回调地址，通知url必须为外网可访问的url，不能携带参数。 公网域名必须为https，如果是走专线接入，使用专线NAT IP或者私有回调域名可使用http
	apiv3_key             string // 用商户平台上设置的APIv3密钥【微信商户平台—>账户设置—>API安全—>设置APIv3密钥】
	serial                string // 商户当前所持有的 微信支付平台证书的序列号
	rsaPrivate_key        string // 商户平台私钥
	certificate_publickey string // 平台证书公钥
}

// 创建微信小程序支付实例
//
//	appid: 由微信生成的应用ID，全局唯一。示例值：wxd678efh567hg6787
//	mchid: 直连商户的商户号，由微信支付生成并下发。示例值：1230000109
//	notify_url: 异步接收微信支付结果通知的回调地址，通知url必须为外网可访问的url，不能携带参数。公网域名必须为https
//	apiv3_key: 用商户平台上设置的APIv3密钥【微信商户平台—>账户设置—>API安全—>设置APIv3密钥】
//	serial: 商户当前所持有的 微信支付平台证书的序列号
//	rsaPrivateKey: 商户平台私钥
//	certificate_publickey: 平台证书公钥
func NewMiniPay(appid, mchid, notify_url, apiv3_key, serial, rsaPrivate_key, certificate_publickey string) *wxminipay {
	if appid == "" || mchid == "" || notify_url == "" || apiv3_key == "" || serial == "" || rsaPrivate_key == "" {
		logutil.Error("初始化微信小程序支付实例失败: 部分参数为空")
		return nil
	}
	var w = wxminipay{
		appid:                 appid,
		mchid:                 mchid,
		notify_url:            notify_url,
		apiv3_key:             apiv3_key,
		serial:                serial,
		rsaPrivate_key:        rsaPrivate_key,
		certificate_publickey: certificate_publickey,
	}
	return &w
}

// 生成预支付交易单
func (w *wxminipay) PrePay(para JsApiPara) (*jsApiResponse, error) {
	var wxPara = jsApiRequest{
		appid:        w.appid,
		mchid:        w.mchid,
		notify_url:   w.notify_url,
		description:  para.Description,
		out_trade_no: para.OutTradeNo,
		attach:       "",
		amount: jsApiAmount{
			total:    para.Total,
			currency: "CNY",
		},
		payer: jsApiPayer{
			openid: para.Openid,
		},
	}
	var postData, _ = jsonutil.ToJson(wxPara)
	var resp, _, err = webutil.Post(jsapi, postData, webutil.JsonContentType)
	if err != nil {
		return nil, err
	} else {
		var result jsApiResponse
		jsonutil.ToModel(resp, &result)
		return &result, nil
	}
}

// 根据微信订单号查询订单信息
//
//	wxOrderId: 微信支付系统生成的订单号
func (w *wxminipay) QueryByWxOrderId(wxOrderId string) (*queryResponse, error) {
	var queryUrl = query1 + wxOrderId + "?mchid=" + w.mchid
	var resp, _, err = webutil.Get(queryUrl)
	if err != nil {
		return nil, err
	} else {
		var result queryResponse
		jsonutil.ToModel(resp, &result)
		return &result, nil
	}
}

// 根据微信订单号查询订单信息
//
//	tradeOrderId: 商户自己生成的订单号
func (w *wxminipay) QueryByTradeId(tradeOrderId string) (*queryResponse, error) {
	var queryUrl = query2 + tradeOrderId + "?mchid=" + w.mchid
	var resp, _, err = webutil.Get(queryUrl)
	if err != nil {
		return nil, err
	} else {
		var result queryResponse
		jsonutil.ToModel(resp, &result)
		return &result, nil
	}
}

// 关闭微信订单
//
//	tradeOrderId: 商户自己生成的订单号
func (w *wxminipay) CloseOrder(tradeOrderId string) error {
	var closeUrl = query2 + tradeOrderId + "/close"
	var postData = fmt.Sprintf(`{"mchid":"%s","out_trade_no":"%s"}`, w.mchid, tradeOrderId)
	var _, _, err = webutil.Post(closeUrl, postData, webutil.JsonContentType)
	return err
}

// 解密通知内容
//
//	resp: 微信通知返回的json
//	headData: 微信通知head中携带的参数
func (w *wxminipay) PayNotify(resp string, headData NotifyHeadData) (*notifyResult, error) {
	if headData.Serial != w.serial {
		return nil, errors.New("通知的序列号与微信支付平台证书的序列号不一致")
	}
	var format = `%s\n%s\n%s\n`
	var str = fmt.Sprintf(format, headData.Timestamp, headData.Nonce, resp)
	var sign, err = encryptutil.Rsa().SignSha256WithRsa(str, w.certificate_publickey)
	if err != nil {
		return nil, err
	}
	var wxSign, _ = encryptutil.Base64Decodeing(headData.Signature)
	if sign != wxSign {
		return nil, errors.New("微信通知签名验证失败")
	}
	var notifyModel notifyResponae
	jsonutil.ToModel(resp, &notifyModel)
	detailJson, err := DecryptAES256GCM(w.apiv3_key, notifyModel.Resource.AssociatedData, notifyModel.Resource.Nonce, notifyModel.Resource.CipherText)
	if err != nil {
		return nil, errors.New("微信通知内容解密失败！" + err.Error())
	}

	var result notifyResult
	if err := jsonutil.ToModel(detailJson, &result); err != nil {
		return nil, err
	}
	return &result, nil
}

// 获取平台证书列表
func (w *wxminipay) GetCertificates() (string, error) {
	var header = make(map[string]string)
	header["Authorization"] = w.SignForCertificates(certificates)
	header["Accept"] = "application/json"
	header["User-Agent"] = "https://zh.wikipedia.org/wiki/User_agent"
	resp := webutil.GetWeb(certificates, header, nil, "", 30)
	if resp.Error != nil {
		return "", resp.Error
	}
	var certificate certificatesResp
	jsonutil.ToModel(resp.GetRespString(), &certificate)
	json, err := DecryptAES256GCM(w.apiv3_key, certificate.Data[0].EncryptCertificate.AssociatedDdata, certificate.Data[0].EncryptCertificate.Nonce, certificate.Data[0].EncryptCertificate.Algorithm)
	if err != nil {
		return "", err
	}
	return json, nil
}

// 商户生成请求的签名
//
//	requestUrl: 请求的地址
func (w *wxminipay) SignForCertificates(requestUrl string) string {
	var timestamp = timeutil.ConvertToTimeStamp(time.Now())
	var randStr = randutil.GetRandChars(32)
	var urlPath = path.Base(requestUrl)
	var format = "GET\n%s\n%d\n%s\n"
	var str = fmt.Sprintf(format, urlPath, timestamp, randStr)
	var sign, _ = encryptutil.Rsa().SignSha256WithRsa(str, w.rsaPrivate_key)
	var signFormat = `WECHATPAY2-SHA256-RSA2048 mchid="%s",nonce_str="%s",signature="%s",timestamp="%v",serial_no="%s"`
	return fmt.Sprintf(signFormat, w.mchid, randStr, sign, timestamp, w.serial)
}

// 申请交易账单
//
//	bill_date: 账单日期 格式yyyy-MM-dd 仅支持三个月内的账单下载申
//	bill_type: 账单类型 枚举值：ALL：返回当日所有订单信息（不含充值退款订单）SUCCESS：返回当日成功支付的订单（不含充值退款订单）REFUND：返回当日退款订单（不含充值退款订单）
//
// 返回已下载到服务器的账单文件地址
func (w *wxminipay) GetTradeBill(bill_date, bill_type string) (string, error) {
	if bill_date == "" || bill_type == "" {
		return "", errors.New("参数错误")
	}
	var requestUrl = fmt.Sprintf("%s?bill_date=%s&bill_type=%s", tradebill, bill_date, bill_type)
	return w.downloadBillFile(requestUrl)
}

// 申请资金账单
//
//	bill_date: 账单日期 格式yyyy-MM-dd 仅支持三个月内的账单下载申
//	account_type: 资金账户类型 不填则默认是BASIC 枚举值：BASIC：基本账户 OPERATION：运营账户FEES：手续费账户
//
// 返回已下载到服务器的账单文件地址
func (w *wxminipay) GetFundFlowBill(bill_date, account_type string) (string, error) {
	if bill_date == "" {
		return "", errors.New("参数错误")
	}
	var requestUrl = fmt.Sprintf("%s?bill_date=%s&account_type=%s", fundflowbill, bill_date, account_type)
	return w.downloadBillFile(requestUrl)
}

// 下载账单
func (w *wxminipay) downloadBillFile(requestUrl string) (string, error) {
	resp, _, err := webutil.Get(requestUrl)
	if err != nil {
		return "", err
	}
	var model tradebillResp
	jsonutil.ToModel(resp, &model)

	// 下载账单
	var header = make(map[string]string)
	header["Authorization"] = w.SignForCertificates(certificates)

	res := webutil.GetWeb(model.DownloadUrl, header, nil, "", 30)
	if res.Error != nil {
		return "", res.Error
	}

	// 保存账单
	fileutil.CreateFolderIfNotExists(tradebillSaveFolder)
	var fileName = timeutil.FormateDateTimeString(time.Now()) + ".xlsx"
	var fullName = path.Join(tradebillSaveFolder, fileName)
	fileutil.WriteFile(fullName, res.RespBytes)
	return "/" + fullName, nil
}
