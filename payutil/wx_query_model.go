package payutil

// wx查询订单返回值
type queryResponse struct {
	Appid          string `json:"appid"`            // 直连商户申请的公众号或移动应用appid
	Mchid          string `json:"mchid"`            // 直连商户的商户号，由微信支付生成并下发
	OoutTradeNo    string `json:"out_trade_no"`     // 商户系统内部订单号，只能是数字、大小写字母_-*且在同一个商户号下唯一	示例值：1217752501201407033233368018
	TransactionId  string `json:"transaction_id"`   // 微信支付系统生成的订单号 示例值：1217752501201407033233368018
	TradeType      string `json:"trade_type"`       // 交易类型，枚举值：	JSAPI：公众号支付	NATIVE：扫码支付	APP：APP支付	MICROPAY：付款码支付	MWEB：H5支付	FACEPAY：刷脸支付
	TradeState     string `json:"trade_state"`      // 易状态，枚举值：	SUCCESS：支付成功	REFUND：转入退款	NOTPAY：未支付	CLOSED：已关闭	REVOKED：已撤销（仅付款码支付会返回）	USERPAYING：用户支付中（仅付款码支付会返回）	PAYERROR：支付失败（仅付款码支付会返回）
	TradeStateDesc string `json:"trade_state_desc"` // 交易状态描述
	BankType       string `json:"bank_type"`        // 付款银行
	Attach         string `json:"attach"`           // 附加数据
	SuccessTime    string `json:"success_time"`     // 支付完成时间，遵循rfc3339标准格式，格式为yyyy-MM-DDTHH:mm:ss+TIMEZONE
	Payer          struct {
		Openid string `json:"openid"` // 用户在直连商户appid下的唯一标识
	} `json:"payer"` // 支付者信息
	Amount struct {
		Total         int    `json:"total"`          // 订单总金额，单位为分
		PayerTotal    int    `json:"payer_total"`    // 用户支付金额，单位为分。（指使用优惠券的情况下，这里等于总金额-优惠券金额）
		Currency      string `json:"currency"`       // CNY：人民币，境内商户号仅支持人民币
		PayerCurrency string `json:"payer_currency"` // 用户支付币种
	} `json:"amount"` // 订单金额信息，当支付成功时返回该字段
	SceneInfo struct {
		DeviceId string `json:"device_id"` // 商户端设备号（发起扣款请求的商户服务器设备号）
	} `json:"scene_info"` // 支付场景描述
}
