package payutil

// 下载证书接口返回值
type certificatesResp struct {
	Data []certificatesItem `json:"data"`
}

// 下载证书接口详情
type certificatesItem struct {
	SerialNo           string `json:"serial_no"`
	EffectiveTime      string `json:"effective_time"`
	ExpireTime         string `json:"expire_time"`
	EncryptCertificate struct {
		Algorithm       string `json:"algorithm"`
		Nonce           string `json:"nonce"`
		AssociatedDdata string `json:"associated_data"`
		CipherText      string `json:"ciphertext"`
	} `json:"encrypt_certificate"`
}
