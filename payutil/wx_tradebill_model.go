package payutil

// 申请交易单返回值
type tradebillResp struct {
	HashType    string `json:"hash_type"`    // 哈希类型	枚举值：SHA1：SHA1值
	HashValue   string `json:"hash_value"`   // 原始账单（gzip需要解压缩）的摘要值，用于校验文件的完整性。
	DownloadUrl string `json:"download_url"` // 供下一步请求账单文件的下载地址，该地址30s内有效。
}
