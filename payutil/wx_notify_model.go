package payutil

// 微信支付通知返回的数据
type notifyResponae struct {
	Id           string `json:"id"`            // 通知的唯一ID
	CreateTime   string `json:"create_time"`   // 通知创建的时间，遵循rfc3339标准格式，格式为yyyy-MM-DDTHH:mm:ss+TIMEZONE
	EventType    string `json:"event_type"`    // 通知的类型，支付成功通知的类型为TRANSACTION.SUCCESS
	ResourceType string `json:"resource_type"` // 通知的资源数据类型，支付成功通知为encrypt-resource
	Summary      string `json:"summary"`       // 回调摘要
	Resource     struct {
		Algorithm      string `json:"algorithm"`       // 对开启结果数据进行加密的加密算法，目前只支持AEAD_AES_256_GCM
		CipherText     string `json:"ciphertext"`      // Base64编码后的开启/停用结果数据密文
		AssociatedData string `json:"associated_data"` // 附加数据
		OriginalType   string `json:"original_type"`   // 原始回调类型，为transaction
		Nonce          string `json:"nonce"`           // 加密使用的随机串
	} `json:"resource"` // 通知资源数据	json格式
}

// 微信支付通知时head中附带的信息
type NotifyHeadData struct {
	Timestamp string
	Nonce     string
	Signature string
	Serial    string
}

// 微信支付通知解密后的最终内容
type notifyResult struct {
	TransactionId string `json:"transaction_id"` //"1217752501201407033233368018",
	Amount        struct {
		PayerTotal    int    `json:"payer_total"`    //100,
		Total         int    `json:"total"`          //100,
		Currency      string `json:"currency"`       //"CNY",
		PayerCurrency string `json:"payer_currency"` //"CNY"
	} `json:"amount"`
	Mchid           string            `json:"mchid"`       //"1230000109",
	TradeState      string            `json:"trade_state"` //"SUCCESS",
	BankType        string            `json:"bank_type"`   //"CMC",
	PromotionDetail []promotionDetail `json:"promotion_detail"`
	SuccessTime     string            `json:"success_time"` //"2018-06-08T10//34//56+08//00",
	Payer           struct {
		Openid string `json:"openid"` //"oUpF8uMuAJO_M2pxb1Q9zNjWeS6o"
	} `json:"payer"`
	OutTradeNo     string `json:"out_trade_no"`     //"1217752501201407033233368018",
	Appid          string `json:"appid"`            //"wxd678efh567hg6787",
	TradeStateDesc string `json:"trade_state_desc"` //"支付成功",
	TradeType      string `json:"trade_type"`       //"MICROPAY",
	Attach         string `json:"attach"`           //"自定义数据",
	SceneInfo      struct {
		DeviceId string `json:"device_id"` //"013467007045764"
	} `json:"scene_info"`
}

type goodsDetail struct {
	GoodsRemark    string `json:"goods_remark"`    //"商品备注信息",
	Quantity       int    `json:"quantity"`        //1,
	DiscountAmount int    `json:"discount_amount"` //1,
	GoodsId        string `json:"goods_id"`        //"M1006",
	UnitPrice      int    `json:"unit_price"`      //100
}

type promotionDetail struct {
	Amount              int           `json:"amount"`               //100,
	WechatpayContribute int           `json:"wechatpay_contribute"` //0,
	CouponId            string        `json:"coupon_id"`            //"109519",
	Scope               string        `json:"scope"`                //"GLOBAL",
	MerchantContribute  int           `json:"merchant_contribute"`  //0,
	Name                string        `json:"name"`                 //"单品惠-6",
	OtherContribute     int           `json:"other_contribute"`     //0,
	Currency            string        `json:"currency"`             //"CNY",
	StockId             string        `json:"stock_id"`             //"931386",
	GoodsDetail         []goodsDetail `json:"goods_detail"`
}
