package payutil

// 预支付参数
type JsApiPara struct {
	Description string `json:"description"`  // 商品描述
	OutTradeNo  string `json:"out_trade_no"` // 商户系统内部订单号，只能是数字、大小写字母_-*且在同一个商户号下唯一	示例值：1217752501201407033233368018
	Attach      string `json:"attach"`       // 附加数据，在查询API和支付通知中原样返回，可作为自定义参数使用，实际情况下只有支付完成状态才会返回该字段
	Total       int    `json:"total"`        // 订单总金额，单位为分
	Openid      string `json:"openid"`       // 用户在直连商户appid下的唯一标识
}

// wx预支付参数
type jsApiRequest struct {
	appid        string      // 由微信生成的应用ID，全局唯一
	mchid        string      // 直连商户的商户号，由微信支付生成并下发
	description  string      // 商品描述
	out_trade_no string      // 商户系统内部订单号，只能是数字、大小写字母_-*且在同一个商户号下唯一	示例值：1217752501201407033233368018
	attach       string      // 附加数据，在查询API和支付通知中原样返回，可作为自定义参数使用，实际情况下只有支付完成状态才会返回该字段
	notify_url   string      // 异步接收微信支付结果通知的回调地址，通知url必须为外网可访问的url，不能携带参数。 公网域名必须为https，如果是走专线接入，使用专线NAT IP或者私有回调域名可使用http
	amount       jsApiAmount // 订单金额信息
	payer        jsApiPayer  // 支付者信息
}

// 订单金额信息
type jsApiAmount struct {
	total    int    // 订单总金额，单位为分
	currency string // CNY：人民币，境内商户号仅支持人民币
}

// 支付者信息
type jsApiPayer struct {
	openid string // 用户在直连商户appid下的唯一标识
}

// wx预支付返回值
type jsApiResponse struct {
	PrepayId string `json:"prepay_id"` // 预支付交易会话标识。用于后续接口调用中使用，该值有效期为2小时
}
