package convertutil

import (
	"encoding/json"
	"strconv"
)

func ConvertStringToInt64(s string) (int64, error) {
	return strconv.ParseInt(s, 10, 64)
}

func ConvertInt64ToString(i int64) string {
	return strconv.FormatInt(i, 10)
}

func ConvertStringToFloat64(s string) (float64, error) {
	return strconv.ParseFloat(s, 64)
}

// 将float64转换为string
//
//	f: float64值
//	prec: 保留小数位数
func ConvertFloat64ToString(f float64, prec uint) string {
	return strconv.FormatFloat(f, 'f', int(prec), 64)
}

// 将对象转换为map
func ConvertStructToMap(data interface{}) map[string]interface{} {
	jsonStr, _ := json.Marshal(&data)
	m := make(map[string]interface{})
	json.Unmarshal(jsonStr, &m)
	return m
}
