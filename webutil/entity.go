package webutil

import "net/http"

type WebResponse struct {
	RespBytes  []byte         `json:"respBytes"`  // 返回的字节内容
	Cookies    []*http.Cookie `json:"cookies"`    // 返回的Cookies
	TimeCost   float64        `json:"timeCost"`   // 请求用时
	StatusCode int            `json:"statusCode"` // 返回的状态码
	Error      error          `json:"error"`      // 错误信息
}

// 获取返回的字符串内容
func (r *WebResponse) GetRespString() string {
	return string(r.RespBytes)
}
