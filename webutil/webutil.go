package webutil

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
	"net/url"
	"os"
	"time"

	"gitee.com/juscc/goutil/errutil"
)

const (
	JsonContentType = "application/json;charset=utf-8"    // JSON
	FormContentType = "application/x-www-form-urlencoded" // 原生form表单
	FileContentType = "multipart/form-data"               // 文件
)

// 发送简单Get请求
func Get(requestUrl string) (respStr string, statusCode int, err error) {
	defer errutil.CatchError()
	resp, err := http.Get(requestUrl)
	if err != nil {
		return
	}
	//用完后立即关闭连接
	resp.Close = true
	defer resp.Body.Close()
	statusCode = resp.StatusCode
	respBytes, _ := io.ReadAll(resp.Body)
	respStr = string(respBytes)
	return
}

// 发送简单Post请求
func Post(requestUrl string, postData string, contentType string) (respStr string, statusCode int, err error) {
	para := bytes.NewBuffer([]byte(postData))
	resp, err := http.Post(requestUrl, contentType, para)
	if err != nil {
		return
	}
	//用完后立即关闭连接
	resp.Close = true
	defer resp.Body.Close()
	statusCode = resp.StatusCode
	respBytes, _ := io.ReadAll(resp.Body)
	respStr = string(respBytes)
	return
}

// 发送复杂Get请求
//
//	requestUrl: 请求地址
//	headers: 请求头
//	cookies: 请求cookies
//	proxy: 代理
//	timeout: 超时时间（秒）
func GetWeb(requestUrl string, headers, cookies map[string]string, proxy string, timeout int) *WebResponse {
	return sendRequestBytes(requestUrl, http.MethodGet, "", headers, cookies, proxy, timeout)
}

// 发送复杂Post请求
//
//	requestUrl: 请求地址
//	postData: 请求的内容
//	headers: 请求头
//	cookies: 请求cookies
//	proxy: 代理
//	timeout: 超时时间（秒）
func PostWeb(requestUrl, postData string, headers, cookies map[string]string, proxy string, timeout int) *WebResponse {
	return sendRequestBytes(requestUrl, http.MethodPost, postData, headers, cookies, proxy, timeout)
}

// 发送Get请求，并返回返回结果模型
//
//	requestUrl: 请求地址
//	headers: 请求头
//	cookies: 请求cookies
//	proxy: 代理
//	timeout: 超时时间（秒）
//	model: 转换模型的地址
func GetWebModel(requestUrl string, headers, cookies map[string]string, proxy string, timeout int, model interface{}) *WebResponse {
	var resp = sendRequestBytes(requestUrl, http.MethodGet, "", headers, cookies, proxy, timeout)
	if resp.Error != nil {
		return resp
	}
	if err := json.Unmarshal(resp.RespBytes, &model); err != nil {
		resp.Error = err
	}
	return resp
}

// 发送Post请求，并返回返回结果模型
//
//	requestUrl: 请求地址
//	postData: 请求的内容
//	headers: 请求头
//	cookies: 请求cookies
//	proxy: 代理
//	timeout: 超时时间（秒）
//	model: 转换模型的地址
func PostWebModel(requestUrl, postData string, headers, cookies map[string]string, proxy string, timeout int, model interface{}) *WebResponse {
	var resp = sendRequestBytes(requestUrl, http.MethodPost, postData, headers, cookies, proxy, timeout)
	if resp.Error != nil {
		return resp
	}
	if err := json.Unmarshal(resp.RespBytes, &model); err != nil {
		resp.Error = err
	}
	return resp
}

// 使用代理发送请求
func sendRequestBytes(requestUrl, method, postData string, headers, cookies map[string]string, proxy string, timeout int) *WebResponse {
	defer errutil.CatchError()
	var result = new(WebResponse)
	var client = &http.Client{}
	//设置超时时间
	if timeout > 0 {
		client.Timeout = time.Second * time.Duration(timeout)
	}
	//是否使用代理
	if proxy != "" {
		var p = func(_ *http.Request) (*url.URL, error) {
			return url.Parse(proxy)
		}
		var transport = &http.Transport{DisableKeepAlives: true, Proxy: p}
		client.Transport = transport
	}
	//转换成postBody
	bytesData := bytes.NewBuffer([]byte(postData))
	req, err := http.NewRequest(method, requestUrl, bytesData)
	if err != nil {
		result.Error = err
		return result
	}
	//用完后立即关闭连接
	req.Close = true
	for k, v := range headers {
		req.Header.Set(k, v)
	}
	for k, v := range cookies {
		req.AddCookie(&http.Cookie{Name: k, Value: v})
	}
	tStart := time.Now()
	resp, err := client.Do(req)
	result.TimeCost = time.Since(tStart).Seconds()
	//请求用时（秒）
	if err != nil {
		result.Error = err
		return result
	}
	defer resp.Body.Close()
	result.StatusCode = resp.StatusCode
	//返回内容
	result.Cookies = resp.Cookies()
	result.RespBytes, _ = io.ReadAll(resp.Body)
	return result
}

// 获取请求 head 的基本参数（User-Agent/Accept/Accept-Language）
func GetBaseHeaders() map[string]string {
	var headers = make(map[string]string)
	headers["User-Agent"] = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/112.0.0.0 Safari/537.36 Edg/112.0.1722.58"
	headers["Accept"] = "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7"
	headers["Accept-Language"] = "zh-CN,zh;q=0.9"
	return headers
}

// 获取请求 head 的基本参数（User-Agent/Accept/Accept-Language），并设置 Content-Type为 json
func GetBaseJsonHeaders() map[string]string {
	var headers = GetBaseHeaders()
	headers["Content-Type"] = JsonContentType
	return headers
}

// 下载文件
//
//	url：下载地址
//	filepath: 文件保存路径
func DownloadFile(url, filepath string) error {
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	out, err := os.Create(filepath)
	if err != nil {
		return err
	}
	defer out.Close()

	_, err = io.Copy(out, resp.Body)

	return err
}
