package logutil

import (
	"fmt"
	"os"
	"path"
	"strconv"
	"strings"
	"sync"
	"time"

	sysdebug "runtime/debug"

	"gitee.com/juscc/goutil/fileutil"
	"gitee.com/juscc/goutil/sliceutil"
	"gitee.com/juscc/goutil/timeutil"
)

// 日志详情模型
type logModel struct {
	logType string
	logMsg  string
	logTime time.Time
}

// 带缓冲区的日志功能模块
type cacheLog struct {
	datas     []logModel    // 日志缓冲区
	mutex     *sync.Mutex   // 日志缓冲区锁
	logFolder string        // 日志文件目录
	interval  time.Duration // 写日志到文件间隔时间
}

// 创建带缓冲区的日志，每间隔一段时间保存一次缓冲区日志到硬盘，适用于写日志频率非常高的情况
//
// 用完请不要立即退出，否则可能存在部分缓冲区日志未写入硬盘的情况
//
//	folder:   日志文件目录
//	interval: 保存日志到文件的间隔时间
func NewCacheLog(folder string, interval time.Duration) *cacheLog {
	var l = new(cacheLog)
	l.logFolder = folder
	l.interval = interval
	l.mutex = new(sync.Mutex)
	l.core()
	return l
}

// 写日志到缓冲区
//
//	logType: 日志类型（用来命名文件）
//	logMsg:  日志内容
func (l *cacheLog) Log(logType, logMsg string) {
	var logItem = logModel{
		logType: logType,
		logMsg:  logMsg,
		logTime: time.Now(),
	}
	l.mutex.Lock()
	l.datas = append(l.datas, logItem)
	l.mutex.Unlock()
}

// 每秒保存一次日志到硬盘
func (l *cacheLog) core() {
	go func() {
		var t = time.NewTicker(l.interval)
		var tmps []logModel
		for range t.C {
			if len(l.datas) > 0 {
				l.mutex.Lock()
				tmps = append(tmps[:0], l.datas...)
				l.datas = l.datas[:0]
				l.mutex.Unlock()
				writeLogsToFile(tmps, l.logFolder)
			}
		}
	}()
}

// 批量写入日志到文件
func writeLogsToFile(datas []logModel, baseFolder string) {
	defer catchError()
	if len(datas) == 0 {
		return
	}
	var now = time.Now()
	var year = strconv.Itoa(now.Year())
	var date = now.Format(time.DateOnly)
	var logFolder = path.Join(baseFolder, year, date)
	fileutil.CreateFolderIfNotExists(logFolder)
	var types = sliceutil.Select(datas, func(item *logModel) string { return item.logType })
	types = sliceutil.Distinct(types)
	for _, logType := range types {
		var fileName = logFolder + "/" + now.Format("2006-01-02.15") + "." + logType + ".txt"
		var typeDatas = sliceutil.WhereReference(datas, func(item *logModel) bool { return item.logType == logType })
		file, _ := os.OpenFile(fileName, os.O_APPEND|os.O_CREATE, 0644)
		var sb strings.Builder
		for _, data := range typeDatas {
			var timeString = data.logTime.Format(timeutil.DateTimeFormatString)
			var lineStr = "#" + timeString + " " + data.logMsg + "\r\n" + "------------------------------------------------------\r\n"
			sb.WriteString(lineStr)
		}
		file.WriteString(sb.String())
		file.Close()
	}
}

// 单独定义，防止循环引用
func catchError() {
	if err := recover(); err != nil {
		stack := string(sysdebug.Stack())
		fmt.Printf("catch error ---> %v\n", err)
		Error(fmt.Sprintf("catch error ---> %v\nstack trace ---> %s", err, stack))
	}
}
