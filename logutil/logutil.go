package logutil

import (
	"log"
	"os"
	"path"
	"strconv"
	"sync"
	"time"
)

var logFileFolder string // 日志文件目录，默认为/Log
var mutex sync.Mutex

type logType int

const (
	info logType = iota
	debug
	warm
	error
	fatal
)

func SetLogFolder(folder string) {
	logFileFolder = folder
}

func Info(msg string) {
	write(msg, info)
}

func Debug(msg string) {
	write(msg, debug)
}

func Warm(msg string) {
	write(msg, warm)
}

func Error(msg string) {
	write(msg, error)
}

func Fatal(msg string) {
	write(msg, fatal)
}

func write(msg string, logType logType) {
	mutex.Lock()
	defer mutex.Unlock()

	if logFileFolder == "" {
		logFileFolder = "Log"
	}

	var now = time.Now()
	var year = now.Year()
	var date = now.Format(time.DateOnly)

	var logFolder = path.Join(logFileFolder, strconv.Itoa(year), date)
	isExist := pathExists(logFolder)
	if !isExist {
		err1 := os.MkdirAll(logFolder, os.ModePerm)
		if err1 != nil {
			log.Println(err1)
			return
		}
	}

	file, err := os.OpenFile(getLogFileName(logType, logFolder), os.O_APPEND|os.O_CREATE, 0644)
	if err == nil {
		defer file.Close()
		_, err2 := file.WriteString(formatMsg(msg))
		if err2 != nil {
			log.Println(err2.Error())
			return
		}
	}
}

func pathExists(path string) bool {
	_, err := os.Stat(path)
	if err == nil {
		return true
	}
	if os.IsNotExist(err) {
		return false
	}
	return false
}

func formatMsg(msg string) string {
	var timeString = time.Now().Format("2006-01-02 15:04:05")
	return "#" + timeString + " " + msg + "\r\n" + "------------------------------------------------------\r\n"
}

func getLogFileName(logType logType, logPath string) string {
	var typeText string
	switch logType {
	case info:
		typeText = "Info"
	case debug:
		typeText = "Debug"
	case warm:
		typeText = "Warm"
	case error:
		typeText = "Error"
	case fatal:
		typeText = "Fatal"
	}

	var fileName = time.Now().Format("2006-01-02.15") + "." + typeText + ".txt"
	return path.Join(logPath, fileName)
}
