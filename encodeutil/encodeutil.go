package encodeutil

import (
	"bytes"
	"io"
	"strconv"
	"strings"
	"unicode/utf8"

	"golang.org/x/text/encoding/simplifiedchinese"
	"golang.org/x/text/transform"
)

func GbkToUtf8(s []byte) ([]byte, error) {
	reader := transform.NewReader(bytes.NewReader(s), simplifiedchinese.GBK.NewDecoder())
	d, e := io.ReadAll(reader)
	if e != nil {
		return nil, e
	}
	return d, nil
}

func Utf8ToGbk(s []byte) ([]byte, error) {
	reader := transform.NewReader(bytes.NewReader(s), simplifiedchinese.GBK.NewEncoder())
	d, e := io.ReadAll(reader)
	if e != nil {
		return nil, e
	}
	return d, nil
}

// 转换中文，防止乱码
func EncodeHtmlBytes(html []byte) []byte {
	if !utf8.Valid(html) {
		bt, _ := simplifiedchinese.GBK.NewDecoder().Bytes(html)
		return bt
	}
	return html
}

// 转换中文，防止乱码
func EncodeHtmlString(html string) string {
	var bytes = []byte(html)
	if !utf8.Valid(bytes) {
		bt, _ := simplifiedchinese.GBK.NewDecoder().Bytes(bytes)
		return string(bt)
	}
	return html
}

// 将字符串中的unicode(\uxxxx)码转换成正常字符。
// 如果报错了，则原样返回
func DecodeUnicodeString(str string) (string, error) {
	res, err := strconv.Unquote(strings.Replace(strconv.Quote(str), `\\u`, `\u`, -1))
	if err != nil {
		return str, err
	}
	return res, nil
}
