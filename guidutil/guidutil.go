package guidutil

import (
	uuid "github.com/satori/go.uuid"
)

// Returns canonical string representation of UUID:
// xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx.
func NewGUID() string {
	return uuid.NewV4().String()
}
