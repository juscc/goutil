package imageutil

import (
	"bytes"
	"errors"
	"image"
	"os"
	"path/filepath"
	"strings"

	"github.com/disintegration/imaging"
)

// 修改图片大小
//
//	sourceImgPath: 原始图片路径
//	newImgPath: 新图片路径
//	newWidth: 新的宽度(如为0则按照高度等比缩放)
//	newHeight: 新的高度(如为0则按照宽度等比缩放)
func ResizeImage(sourceImgPath, newImgPath string, newWidth, newHeight int) error {
	imgData, err := os.ReadFile(sourceImgPath)
	if err != nil {
		return err
	}
	buf := bytes.NewBuffer(imgData)
	image, err := imaging.Decode(buf)
	if err != nil {
		return err
	}
	image = imaging.Resize(image, newWidth, newHeight, imaging.Lanczos)
	var dir = filepath.Dir(newImgPath)
	if err := os.MkdirAll(dir, os.ModePerm); err != nil {
		return err
	}
	err = imaging.Save(image, newImgPath)
	if err != nil {
		return err
	}
	return nil
}

// 如果图片宽度超过 newWidth 则修改为 newWidth, 高度等比缩放
//
//	sourceImgPath: 原始图片路径
//	newWidth: 新的宽度
func ResizeImageIfLarge(sourceImgPath string, newWidth int) error {
	imgData, err := os.ReadFile(sourceImgPath)
	if err != nil {
		return err
	}
	buf := bytes.NewBuffer(imgData)
	image, err := imaging.Decode(buf)
	if err != nil {
		return err
	}
	if image.Bounds().Dx() > newWidth {
		image = imaging.Resize(image, newWidth, 0, imaging.Lanczos)
		var dir = filepath.Dir(sourceImgPath)
		if err := os.MkdirAll(dir, os.ModePerm); err != nil {
			return err
		}
		err = imaging.Save(image, sourceImgPath)
		if err != nil {
			return err
		}
	}
	return nil
}

// 如果图片宽度超过 newWidth 则修改为 newWidth, 高度等比缩放
//
//	sourceImgPath: 原始图片路径
//	newImgPath: 新图片路径
//	newWidth: 新的宽度
func ResizeImageIfLargeToNewFile(sourceImgPath string, newImgPath string, newWidth int) error {
	imgData, err := os.ReadFile(sourceImgPath)
	if err != nil {
		return err
	}
	buf := bytes.NewBuffer(imgData)
	image, err := imaging.Decode(buf)
	if err != nil {
		return err
	}
	if image.Bounds().Dx() > newWidth {
		image = imaging.Resize(image, newWidth, 0, imaging.Lanczos)
		var dir = filepath.Dir(newImgPath)
		if err := os.MkdirAll(dir, os.ModePerm); err != nil {
			return err
		}
		err = imaging.Save(image, newImgPath)
		if err != nil {
			return err
		}
	}
	return nil
}

// 获取图片尺寸
//
//	imagePath: 图片路径
func GetImageSize(imagePath string) (image.Point, error) {
	var size image.Point
	if imagePath == "" {
		return size, errors.New("image path cannot be empty")
	}
	imagePath = strings.TrimLeft(imagePath, "/")
	imgData, err := os.ReadFile(imagePath)
	if err != nil {
		return size, err
	}
	buf := bytes.NewBuffer(imgData)
	image, err := imaging.Decode(buf)
	if err != nil {
		return size, err
	}
	return image.Bounds().Size(), nil
}
