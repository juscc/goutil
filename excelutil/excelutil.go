package excelutil

import (
	"fmt"
	"path"
	"strconv"
	"time"

	"gitee.com/juscc/goutil/custypeutil"

	"gitee.com/juscc/goutil/fileutil"
	"gitee.com/juscc/goutil/sliceutil"
	"github.com/gogf/gf/util/gconv"
	"github.com/tealeg/xlsx"
	"github.com/xuri/excelize/v2" //文档 https://xuri.me/excelize/zh-hans/
)

func ReadExcel(filePath string, isFirstRowTitle bool) []map[string]interface{} {
	var result = []map[string]interface{}{}
	//打开文件路径
	xlsxFile, err := xlsx.OpenFile(filePath)
	if err != nil {
		fmt.Println(err)
		return result
	}
	//读取每一个sheet
	//for x := range xlsxFile.Sheets {
	var sheet = xlsxFile.Sheets[0]
	if len(sheet.Rows) == 0 {
		return result
	}

	var titles = getExcelTitle(sheet.Rows, isFirstRowTitle)

	//读取每个sheet下面的行数据
	for y := range sheet.Rows {
		if isFirstRowTitle && y == 0 {
			continue
		}
		var row = sheet.Rows[y]
		//读取每个cell的内容
		var rowMap = make(map[string]interface{})
		for i, cell := range row.Cells {
			var title = titles[i]
			if cell.Type() == xlsx.CellTypeNumeric {
				if rowMap[title], err = cell.Int64(); err != nil {
					rowMap[title], _ = cell.Float()
				}
			} else if cell.Type() == xlsx.CellTypeDate { // 日期格式貌似没起作用
				if rowMap[title], err = cell.GetTime(false); err != nil {
					fmt.Println(err)
				}
			} else if cell.Type() == xlsx.CellTypeBool {
				rowMap[title] = cell.Bool()
			} else {
				rowMap[title] = cell.Value
			}
		}
		if len(rowMap) > 0 {
			result = append(result, rowMap)
		}
	}
	//}
	return result
}

func getExcelTitle(rows []*xlsx.Row, isFirstRowTitle bool) map[int]string {
	var rowMap = make(map[int]string)
	if len(rows) == 0 {
		return rowMap
	} else {
		for i := range rows[0].Cells {
			if isFirstRowTitle {
				rowMap[i] = rows[0].Cells[i].Value
			} else {
				rowMap[i] = strconv.Itoa(i)
			}
		}
		return rowMap
	}
}

// 读取EXCEL内容
//
//	filename: 指定文件路径名称，例如: "D:/aa.xlsx"
//	sheetName: sheet名称，不传默认第一个
func ReadFromExcel(filename string, sheetName string) ([][]string, error) {
	f, err := excelize.OpenFile(filename) //
	if err != nil {
		return nil, err
	}
	defer f.Close()
	firstSheet := ""
	if len(sheetName) > 0 {
		firstSheet = sheetName
	} else {
		firstSheet = f.GetSheetName(0)
	}
	rows, err := f.GetRows(firstSheet)
	return rows, err
}

// 往EXCEL写入内容
//
//	filename: Excel文件保存位置，如: "D:/test.xlsx"，目录若不存在会自动创建
//	datas: 内容
//	sheetName: sheet名称，为空则默认Sheet1
func WriteArrayToExcel(filename string, datas [][]string, sheetName string) error {
	f := excelize.NewFile()
	defer f.Close()
	//默认保存开始名称
	firstSheet := "Sheet1"
	if len(sheetName) > 0 {
		firstSheet = sheetName
	}

	index, err := f.NewSheet(firstSheet)
	if err != nil {
		return err
	}
	for i := 0; i < len(datas); i++ {
		for k, v := range datas[i] { //列
			path, err := excelize.ColumnNumberToName(k + 1)
			if err != nil {
				return err
			}
			err = f.SetCellValue(firstSheet, path+gconv.String(i+1), v)
			if err != nil {
				return err
			}
		}
	}

	f.SetActiveSheet(index)
	if err := f.SaveAs(filename); err != nil {
		return err
	}
	return nil
}

// 将 []map[string]interface{} 数据写入 EXCEL
//
//	filename: Excel文件保存位置，如: "D:/test.xlsx"，目录若不存在会自动创建
//	datas: 要保存的数据内容
//	sheetName: sheet名称，为空则默认Sheet1
//	columns: 列信息
func WriteMapToExcel(filename string, datas []map[string]interface{}, sheetName string, columns []ExcelColumn) error {
	f := excelize.NewFile()
	defer f.Close()

	//默认保存开始名称
	firstSheet := "Sheet1"
	if len(sheetName) > 0 {
		firstSheet = sheetName
	}

	index, err := f.NewSheet(firstSheet)
	if err != nil {
		return err
	}

	var titles = sliceutil.Select(columns, func(item *ExcelColumn) string { return item.Title })
	// write titles
	for columnIndex, column := range titles {
		path, err := excelize.ColumnNumberToName(columnIndex + 1)
		if err != nil {
			return err
		}
		if err := f.SetCellValue(firstSheet, path+gconv.String(1), column); err != nil {
			return err
		}
	}

	// set title bold style
	boldStyle, _ := f.NewStyle(&excelize.Style{Font: &excelize.Font{Bold: true}})
	f.SetRowStyle(firstSheet, 1, 1, boldStyle)

	// freeze first row
	f.SetPanes(firstSheet, &excelize.Panes{Freeze: true, Split: false, XSplit: 0, YSplit: 1, TopLeftCell: "A1", ActivePane: "bottomLeft"})

	// write datas
	var fields = sliceutil.Select(columns, func(item *ExcelColumn) string { return item.Field })
	for rowIndex := 0; rowIndex < len(datas); rowIndex++ {
		for columnIndex, field := range fields {
			path, err := excelize.ColumnNumberToName(columnIndex + 1)
			if err != nil {
				return err
			}
			var cellTag = path + gconv.String(rowIndex+2)
			var value = datas[rowIndex][field]
			switch t := value.(type) {
			case time.Time:
				if t.Hour() == 0 && t.Minute() == 0 && t.Second() == 0 {
					err = f.SetCellValue(firstSheet, cellTag, t.Format(time.DateOnly))
				} else {
					err = f.SetCellValue(firstSheet, cellTag, t.Format(time.DateTime))
				}
			case custypeutil.JsonDate:
				err = f.SetCellValue(firstSheet, cellTag, t.Format(time.DateOnly))
			case custypeutil.JsonDateTime:
				err = f.SetCellValue(firstSheet, cellTag, t.Format(time.DateTime))
			default:
				err = f.SetCellValue(firstSheet, cellTag, t)
			}
			if err != nil {
				return err
			}
		}
	}

	// create folder if not exists
	var saveFolder = path.Dir(filename)
	fileutil.CreateFolderIfNotExists(saveFolder)

	f.SetActiveSheet(index)
	if err := f.SaveAs(filename); err != nil {
		return err
	}
	return nil
}
