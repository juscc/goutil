package excelutil

type ExcelColumn struct {
	Title string // 列显示文本
	Field string // 列字段名
}
