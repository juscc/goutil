package excelutil

import (
	"errors"
	"path"
	"reflect"
	"time"

	"gitee.com/juscc/goutil/custypeutil"
	"gitee.com/juscc/goutil/fileutil"
	"github.com/gogf/gf/util/gconv"
	"github.com/xuri/excelize/v2"
)

// 创建将struct数组导出到Excel的实例
func NewExcelExporter() *excelExport {
	return &excelExport{}
}

type excelTag struct {
	Field string
	Text  string
}

type excelExport struct{}

// 将 struct 数组导出到 Excel
//
//	filename: Excel文件保存位置，如: "D:/test.xlsx"，目录若不存在会自动创建
//	datas: 要导出到Excel的内容（必须为struct数组）
//	sheetName: sheet名称，为空则默认Sheet1
func (e *excelExport) Export(filename string, datas interface{}, sheetName string) error {
	var t = reflect.TypeOf(datas)
	var v = reflect.ValueOf(datas)
	if t.Kind() != reflect.Slice {
		return errors.New("export datas must be slice")
	}
	if t.Elem().Kind() != reflect.Struct {
		return errors.New("slice item must be struct")
	}
	var excelFields = e.getExcelFields(t.Elem())
	if len(excelFields) == 0 {
		return errors.New("no fields to export")
	}

	f := excelize.NewFile()
	defer f.Close()

	//默认保存开始名称
	firstSheet := "Sheet1"
	if len(sheetName) > 0 {
		firstSheet = sheetName
	}
	index, err := f.NewSheet(firstSheet)
	if err != nil {
		return err
	}

	// write titles
	for columnIndex, columntInfo := range excelFields {
		path, err := excelize.ColumnNumberToName(columnIndex + 1)
		if err != nil {
			return err
		}
		if err := f.SetCellValue(firstSheet, path+gconv.String(1), columntInfo.Text); err != nil {
			return err
		}
	}

	// set title bold style
	boldStyle, _ := f.NewStyle(&excelize.Style{Font: &excelize.Font{Bold: true}})
	f.SetRowStyle(firstSheet, 1, 1, boldStyle)

	// freeze first row
	f.SetPanes(firstSheet, &excelize.Panes{Freeze: true, Split: false, XSplit: 0, YSplit: 1, TopLeftCell: "A1", ActivePane: "bottomLeft"})

	// write datas
	for rowIndex := 0; rowIndex < v.Len(); rowIndex++ {
		var item = v.Index(rowIndex)
		for columnIndex, field := range excelFields {
			var value = item.FieldByName(field.Field).Interface()
			path, err := excelize.ColumnNumberToName(columnIndex + 1)
			if err != nil {
				return err
			}
			var cellTag = path + gconv.String(rowIndex+2)
			switch t := value.(type) {
			case time.Time:
				if t.Hour() == 0 && t.Minute() == 0 && t.Second() == 0 {
					err = f.SetCellValue(firstSheet, cellTag, t.Format(time.DateOnly))
				} else {
					err = f.SetCellValue(firstSheet, cellTag, t.Format(time.DateTime))
				}
			case custypeutil.JsonDate:
				err = f.SetCellValue(firstSheet, cellTag, t.Format(time.DateOnly))
			case custypeutil.JsonDateTime:
				err = f.SetCellValue(firstSheet, cellTag, t.Format(time.DateTime))
			default:
				err = f.SetCellValue(firstSheet, cellTag, t)
			}
			if err != nil {
				return err
			}
		}
	}
	// create folder if not exists
	var saveFolder = path.Dir(filename)
	fileutil.CreateFolderIfNotExists(saveFolder)

	f.SetActiveSheet(index)
	if err := f.SaveAs(filename); err != nil {
		return err
	}
	return nil
}

// 获取需要导出到EXCECL的字段名与展示标题列表
//
//	itemType: 数据项类型
func (e excelExport) getExcelFields(itemType reflect.Type) []excelTag {
	var excelFields []excelTag
	for i := 0; i < itemType.NumField(); i++ {
		var field = itemType.Field(i)
		var tag = field.Tag.Get("excel")
		if tag != "" {
			if field.Type.Kind() == reflect.Struct {
				var typeName = field.Type.Name()
				if typeName == "Time" || typeName == "JsonDate" || typeName == "JsonDateTime" {
					excelFields = append(excelFields, excelTag{Field: field.Name, Text: tag})
				} else {
					excelFields = append(excelFields, e.getExcelFields(field.Type)...)
				}
			} else {
				excelFields = append(excelFields, excelTag{Field: field.Name, Text: tag})
			}
		}
	}
	return excelFields
}
