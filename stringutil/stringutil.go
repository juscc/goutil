package stringutil

import (
	"strings"
	"unicode"
)

// 截取字符串
func SubString(source string, startIndex int, length int) string {
	if source == "" {
		return source
	} else {
		if length < 0 {
			return ""
		}
		var lastIndex = startIndex + length
		if lastIndex > len(source) {
			lastIndex = len(source)
		}
		return source[startIndex:lastIndex]
	}
}

// 截取字符串（转换为rune后截取）
func SubStringRune(source string, startIndex int, length int) string {
	if source == "" {
		return source
	} else {
		if length < 0 {
			return ""
		}
		arry := []rune(source)
		var lastIndex = startIndex + length
		if lastIndex > len(arry) {
			lastIndex = len(arry)
		}
		s := arry[startIndex:lastIndex]
		return string(s)
	}
}

// 字符串反转
func Reverse(s string) string {
	r := []rune(s)
	for i, j := 0, len(r)-1; i < j; i, j = i+1, j-1 {
		r[i], r[j] = r[j], r[i]
	}
	return string(r)
}

// 驼峰命名法（首字母小写）
//
//	s: 以_分隔
func CamelCase(s string) string {
	return camelCase(s, "l")
}

// Pascal命名法（首字母大写）
//
//	s: 以_分隔
func PascalCase(s string) string {
	return camelCase(s, "u")
}

// 驼峰命名法
//
//	s: 原字符串
//	f: u-首字母大写 l-首字母小写
func camelCase(s, f string) string {
	var sb strings.Builder
	var isSplitChar = false
	for i, c := range s {
		if i == 0 {
			if f == "l" {
				sb.WriteRune(unicode.ToLower(c))
			} else {
				sb.WriteRune(unicode.ToUpper(c))
			}
		} else if c == '_' {
			isSplitChar = true
		} else {
			if isSplitChar {
				isSplitChar = false
				sb.WriteRune(unicode.ToUpper(c))
			} else {
				sb.WriteRune(c)
			}
		}
	}
	return sb.String()
}

// 首字母转小写
func FirstLower(s string) string {
	var arr = []rune(s)
	arr[0] = unicode.ToLower(arr[0])
	return string(arr)
}

// 首字母转大写
func FirstUpper(s string) string {
	var arr = []rune(s)
	arr[0] = unicode.ToUpper(arr[0])
	return string(arr)
}

// 计算两个字符串的相似度
//
//	word1: 第一个字符串
//	word2: 第二个字符串
//	返回值:
//	两个字符串的距离，表示两个字符串经过多少次变换，可以变成同一个字符串
//	两个字符串的相似度，范围为[0, 1]
func Similarity(word1, word2 string) (distance int, similarity float64) {
	// 内部方法，用于计算最小值、最大值
	min := func(nums ...int) int {
		_min := nums[0]
		for _, v := range nums {
			if v < _min {
				_min = v
			}
		}
		return _min
	}
	max := func(nums ...int) int {
		_max := nums[0]
		for _, v := range nums {
			if v > _max {
				_max = v
			}
		}
		return _max
	}

	// 如果有单词为空，或者单词相同，则直接计算出结果，无需进一步计算
	m, n := len(word1), len(word2)
	if m == 0 {
		distance = n
		return
	}
	if n == 0 {
		distance = m
		return
	}
	if m == n && word1 == word2 {
		distance = 0
		similarity = 1
		return
	}

	// 使用动态规划计算编辑距离(Edit Distance)
	// Step 1: define the data structure
	dp := make([][]int, m+1)
	for i := 0; i <= m; i++ {
		dp[i] = make([]int, n+1)
	}

	// Step 2: init the data
	for i := 0; i <= m; i++ {
		for j := 0; j <= n; j++ {
			if i == 0 {
				dp[i][j] = j
			}
			if j == 0 {
				dp[i][j] = i
			}
		}
	}

	// Step 3: state transition equation
	for i := 1; i <= m; i++ {
		for j := 1; j <= n; j++ {
			if word1[i-1] == word2[j-1] {
				dp[i][j] = dp[i-1][j-1]
			} else {
				dp[i][j] = min(dp[i-1][j], dp[i][j-1], dp[i-1][j-1]) + 1
			}
		}
	}

	// 得到距离并计算相似度
	distance = dp[m][n]
	maxLength := max(m, n)
	similarity = float64(maxLength-distance) / float64(maxLength)

	return
}
