package encryptutil

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/hex"
	"encoding/pem"
	"errors"
	"os"
	"strings"

	"gitee.com/juscc/goutil/fileutil"
	"github.com/wenzhenxi/gorsa"
)

type rsautil struct {
}

func Rsa() rsautil {
	return rsautil{}
}

const (
	// 公钥前缀
	publicPrefix = "-----BEGIN Public key-----"
	// 公钥后缀
	publicSuffix = "-----END Public key-----"
	// 私钥前缀
	privatePrefix = "-----BEGIN Private key-----"
	// 私钥后缀
	privateSuffix = "-----END Private key-----"
)

// 公钥加密
func (s rsautil) PublicEncrypt(data, publicKey string) (string, error) {
	publicKey = validPublic(publicKey)
	return gorsa.PublicEncrypt(data, publicKey)
}

// 公钥解密
func (s rsautil) PublicDecrypt(data, publicKey string) (string, error) {
	publicKey = validPublic(publicKey)
	if d, err := gorsa.PublicDecrypt(data, publicKey); err != nil {
		return "", err
	} else {
		h, _ := hex.DecodeString(d)
		return string(h), nil
	}
}

// 私钥加密
func (s rsautil) PriKeyEncrypt(data, privateKey string) (string, error) {
	privateKey = validPrivate(privateKey)
	return gorsa.PriKeyEncrypt(data, privateKey)
}

// 私钥解密
func (s rsautil) PriKeyDecrypt(data, privateKey string) (string, error) {
	privateKey = validPrivate(privateKey)
	if d, err := gorsa.PriKeyDecrypt(data, privateKey); err != nil {
		return "", err
	} else {
		h, _ := hex.DecodeString(d)
		return string(h), nil
	}
}

// 使用RSAWithMD5算法签名
//
//	data: 原文
//	privateKey: 私钥
func (s rsautil) SignMd5WithRsa(data, privateKey string) (string, error) {
	privateKey = validPrivate(privateKey)
	return gorsa.SignMd5WithRsa(data, privateKey)
}

// 使用RSAWithMD5验证签名
//
//	data: 原文
//	signData: 签名信息
//	publicKey: 公钥
func (s rsautil) VerifySignMd5WithRsa(data, signData, publicKey string) error {
	publicKey = validPublic(publicKey)
	return gorsa.VerifySignMd5WithRsa(data, signData, publicKey)
}

// 使用RSAWithSHA1算法签名
//
//	data: 原文
//	privateKey: 私钥
func (s rsautil) SignSha1WithRsa(data, privateKey string) (string, error) {
	privateKey = validPrivate(privateKey)
	return gorsa.SignSha1WithRsa(data, privateKey)
}

// 使用RSAWithSHA1验证签名
//
//	data: 原文
//	signData: 签名信息
//	publicKey: 公钥
func (s rsautil) VerifySignSha1WithRsa(data, signData, publicKey string) error {
	publicKey = validPublic(publicKey)
	return gorsa.VerifySignSha1WithRsa(data, signData, publicKey)
}

// 使用RSAWithSHA256算法签名
//
//	data: 原文
//	privateKey: 私钥
func (s rsautil) SignSha256WithRsa(data, privateKey string) (string, error) {
	privateKey = validPrivate(privateKey)
	return gorsa.SignSha256WithRsa(data, privateKey)
}

// 使用RSAWithSHA256验证签名
//
//	data: 原文
//	signData: 签名信息
//	publicKey: 公钥
func (s rsautil) VerifySignSha256WithRsa(data, signData, publicKey string) error {
	publicKey = validPublic(publicKey)
	return gorsa.VerifySignSha256WithRsa(data, signData, publicKey)
}

// 修正公钥格式
func validPublic(publicKey string) string {
	if !strings.HasPrefix(publicKey, publicPrefix) {
		publicKey = publicPrefix + "\n" + publicKey
	}
	if !strings.HasSuffix(publicKey, publicSuffix) {
		publicKey = publicKey + "\n" + publicSuffix
	}
	return publicKey
}

// 修正私钥格式
func validPrivate(privateKey string) string {
	if !strings.HasPrefix(privateKey, privatePrefix) {
		privateKey = privatePrefix + "\n" + privateKey
	}
	if !strings.HasSuffix(privateKey, privateSuffix) {
		privateKey = privateKey + "\n" + privateSuffix
	}
	return privateKey
}

// 生成密钥对并保存到文件中
//
//	savePath: 密钥文件存放的目录
func (s rsautil) GenerateRSAKey(savePath string) error {
	if savePath == "" {
		return errors.New("savePath不能为空")
	}
	fileutil.CreateFolderIfNotExists(savePath)
	// 1、RSA生成私钥文件的核心步骤：
	// 1) 生成RSA密钥对
	var bits = 1024
	privateKer, err := rsa.GenerateKey(rand.Reader, bits)
	if err != nil {
		return err
	}
	// 2) 将私钥对象转换成DER编码形式
	derPrivateKer := x509.MarshalPKCS1PrivateKey(privateKer)
	// 3) 创建私钥pem文件
	file, err := os.Create(savePath + "/private.pem")
	if err != nil {
		return err
	}
	// 4) 对密钥信息进行编码，写入到私钥文件中
	block := &pem.Block{
		Type:  "RSA PRIVATE KEY",
		Bytes: derPrivateKer,
	}
	err = pem.Encode(file, block)
	if err != nil {
		return err
	}

	// 2、RSA生成公钥文件的核心步骤：
	// 1) 生成公钥对象
	publicKey := &privateKer.PublicKey
	// 2) 将公钥对象序列化为DER编码格式
	derPublicKey, err := x509.MarshalPKIXPublicKey(publicKey)
	if err != nil {
		return err
	}
	// 3) 创建公钥pem文件
	file, err = os.Create(savePath + "/public.pem")
	if err != nil {
		return err
	}
	// 4) 对公钥信息进行编码，写入到公钥文件中
	block = &pem.Block{
		Type:  "PUBLIC KEY",
		Bytes: derPublicKey,
	}
	err = pem.Encode(file, block)
	if err != nil {
		return err
	}

	return nil
}
