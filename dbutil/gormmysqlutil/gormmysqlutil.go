package gormmysqlutil

import (
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

// https://gorm.io/zh_CN/docs/connecting_to_the_database.html

// 连接 mysql 数据库（不必反复调用）
//
//	connString: root:tX8eS01!PiW7@tcp(localhost:3306)/promotioncenter?charset=utf8mb4&parseTime=True&loc=Local
func NewGormMysqlPool(connString string) (*gorm.DB, error) {
	return gorm.Open(mysql.Open(connString), &gorm.Config{})
}
