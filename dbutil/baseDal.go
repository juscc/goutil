package dbutil

import (
	"database/sql"
	"log"
	"time"
)

// https://github.com/golang/go/wiki/SQLDrivers

// 请勿直接使用
type DbHelper struct {
	db               *sql.DB // 数据库连接池
	sqlType          string  // 数据库类型
	connectionString string  // 数据库连接字符串
}

const (
	// Mysql连接字符串示例
	ConnStringExample = "root:tX8eS01!PiW7@tcp(localhost:3306)/promotioncenter?charset=utf8mb4&parseTime=True&loc=Local"
	// Mysql查询所有表信息SQL
	GetAllTablesSql = "SELECT TABLE_NAME,TABLE_COMMENT FROM information_schema.TABLES WHERE table_schema = '%s'"
	// Mysql查询表的所有字段信息SQL
	GetAllColumnsSql = "SELECT TABLE_NAME,COLUMN_NAME,ORDINAL_POSITION,COLUMN_KEY,DATA_TYPE,IS_NULLABLE,COLUMN_COMMENT FROM information_schema.COLUMNS WHERE table_name = '%s' AND table_schema = '%s'"
)

/**

sql.DB连接池是如何工作的呢?
需要理解的最重要一点是，sql.DB池包含两种类型的连接——“正在使用”连接和“空闲”连接。
当您使用连接执行数据库任务(例如执行SQL语句或查询行)时，该连接被标记为正在使用，任务完成后，该连接被标记为空闲。
当您使用Go执行数据库操作时，它将首先检查池中是否有可用的空闲连接。
如果有可用的连接，那么Go将重用这个现有连接，并在任务期间将其标记为正在使用。
如果在您需要空闲连接时池中没有空闲连接，那么Go将创建一个新的连接。
当Go重用池中的空闲连接时，与该连接有关的任何问题都会被优雅地处理。
异常连接将在放弃之前自动重试两次，这时Go将从池中删除异常连接并创建一个新的连接来执行该任务。
链接：https://www.jianshu.com/p/cbfc398bd4d6

*/

// 创建一个线程安全的数据库连接池（不可频繁创建，全局唯一即可）
//
// 不可直接使用，你应使用具体数据库的util
//
//	dbType: mysql/sqlite3/postgres/oracle/sqlserver
//	connString: 数据库连接字符串
func NewDBPool(dbType string, connString string) (*DbHelper, error) {
	var db = &DbHelper{
		sqlType:          dbType,
		connectionString: connString,
	}
	if err := db.open(); err != nil {
		return nil, err
	}
	return db, nil
}

// 打开数据库连接
func (p *DbHelper) open() error {
	if p.db == nil || p.db.Stats().OpenConnections == 0 {
		var err error
		//使用sql.Open()创建一个空连接池
		p.db, err = sql.Open(p.sqlType, p.connectionString)
		if err != nil {
			return err
		}
		p.db.SetMaxOpenConns(20)           // 设置连接池最大连接数（使用中 + 空闲连接）
		p.db.SetMaxIdleConns(20)           // 设置最大空闲连接数
		p.db.SetConnMaxIdleTime(time.Hour) // 设置单个连接的最大空闲时间，超时后将被清理
		return p.db.Ping()
	}
	return nil
}

// 关闭数据库连接
func (p *DbHelper) Close() error {
	if p.db != nil {
		return p.db.Close()
	}
	return nil
}

// 需要手动关闭数据库连接
func (p *DbHelper) ExeNonQuery(sql string) (int64, error) {
	res, err := p.db.Exec(sql)
	if err != nil {
		errMsg := "sql error:" + err.Error() + "\r\nsql: " + sql
		log.Println(errMsg)
		return 0, err
	}
	rowsAffected, _ := res.RowsAffected()
	return rowsAffected, nil
}

// 需要手动关闭数据库连接
func (p *DbHelper) Query(sql string) (*sql.Rows, error) {
	rows, err := p.db.Query(sql)
	if err != nil {
		errMsg := "sql error:" + err.Error() + "\r\nsql: " + sql
		log.Println(errMsg)
		return nil, err
	}
	return rows, nil
}

// 需要手动关闭数据库连接
func (p *DbHelper) QueryRow(sql string) *sql.Row {
	return p.db.QueryRow(sql)
}

// 预处理
func (p *DbHelper) Prepare(sql string) (*sql.Stmt, error) {
	return p.db.Prepare(sql)
}

// 开始事务
func (p *DbHelper) Begin() (*sql.Tx, error) {
	return p.db.Begin()
}

// 测试连接是否正常
func (p *DbHelper) Ping() error {
	return p.db.Ping()
}

// 设置连接池最大同时打开的连接数 (使用中 + 空闲)
//
//	n: 0表示没有限制
func (p *DbHelper) SetMaxOpenConns(n int) {
	p.db.SetMaxOpenConns(n)
}

// 设置连接池最大空闲连接数
//
//	n: 0表示不保留任何空闲链接
func (p *DbHelper) SetMaxIdleConns(n int) {
	p.db.SetMaxIdleConns(n)
}

// 设置一个连接最大生存时间，默认连接的存活时间没有限制，永久可用。
func (p *DbHelper) SetConnMaxLifetime(d time.Duration) {
	p.db.SetConnMaxLifetime(d)
}

// 设置一个连接在被标记为失效之前最长空闲时间
func (p *DbHelper) SetConnMaxIdleTime(d time.Duration) {
	p.db.SetConnMaxIdleTime(d)
}
