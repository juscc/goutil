package oracleutil

import (
	"gitee.com/juscc/goutil/dbutil"
	_ "github.com/sijms/go-ora" // oracle
)

func NewDBPool(connString string) (*dbutil.DbHelper, error) {
	return dbutil.NewDBPool("oracle", connString)
}
