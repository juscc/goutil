package gormsqliteutil

import (
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

// https://gorm.io/zh_CN/docs/connecting_to_the_database.html

// 连接 sqlite 数据库（不必反复调用）
//
//	dbFilePath: dbfile.db
func NewGormSqlitePool(dbFilePath string) (*gorm.DB, error) {
	return gorm.Open(sqlite.Open(dbFilePath), &gorm.Config{})
}
