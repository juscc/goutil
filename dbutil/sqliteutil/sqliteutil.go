package sqliteutil

import (
	"gitee.com/juscc/goutil/dbutil"
	_ "github.com/mattn/go-sqlite3" // sqlite3
)

func NewDBPool(connString string) (*dbutil.DbHelper, error) {
	return dbutil.NewDBPool("sqlite3", connString)
}
