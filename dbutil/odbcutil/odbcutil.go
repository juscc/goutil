package odbcutil

import (
	"gitee.com/juscc/goutil/dbutil"
	_ "github.com/alexbrainman/odbc"
)

func NewDBPool(connString string) (*dbutil.DbHelper, error) {
	return dbutil.NewDBPool("odbc", connString)
}
