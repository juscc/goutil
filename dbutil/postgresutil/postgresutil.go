package postgresutil

import (
	"gitee.com/juscc/goutil/dbutil"
	_ "github.com/lib/pq" // postgres
)

func NewDBPool(connString string) (*dbutil.DbHelper, error) {
	return dbutil.NewDBPool("postgres", connString)
}
