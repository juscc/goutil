package gormpostgresutil

import (
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

// https://gorm.io/zh_CN/docs/connecting_to_the_database.html

// 连接 postgres 数据库（不必反复调用）
//
//	connString: host=localhost user=gorm password=gorm dbname=gorm port=9920 sslmode=disable TimeZone=Asia/Shanghai
func NewGormPostgresPool(connString string) (*gorm.DB, error) {
	return gorm.Open(postgres.Open(connString), &gorm.Config{})
}
