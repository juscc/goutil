package gormsqlserverutil

import (
	"gorm.io/driver/sqlserver"
	"gorm.io/gorm"
)

// https://gorm.io/zh_CN/docs/connecting_to_the_database.html

// 连接 sqlserver 数据库（不必反复调用）
//
//	connString: sqlserver://gorm:LoremIpsum86@localhost:9930?database=gorm
func NewGormSqlServerPool(connString string) (*gorm.DB, error) {
	return gorm.Open(sqlserver.Open(connString), &gorm.Config{})
}
