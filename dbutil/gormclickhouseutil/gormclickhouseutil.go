package gormclickhouseutil

import (
	"gorm.io/driver/clickhouse"
	"gorm.io/gorm"
)

// https://gorm.io/zh_CN/docs/connecting_to_the_database.html

// 连接 clickhouse 数据库（不必反复调用）
//
//	connString: tcp://localhost:9000?database=gorm&username=gorm&password=gorm&read_timeout=10&write_timeout=20
func NewGormClickHousePool(connString string) (*gorm.DB, error) {
	return gorm.Open(clickhouse.Open(connString), &gorm.Config{})
}
