package sqlserverutil

import "gitee.com/juscc/goutil/dbutil"

func NewDBPool(connString string) (*dbutil.DbHelper, error) {
	return dbutil.NewDBPool("sqlserver", connString)
}
