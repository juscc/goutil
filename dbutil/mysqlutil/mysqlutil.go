package mysqlutil

import (
	"gitee.com/juscc/goutil/dbutil"
	_ "github.com/go-sql-driver/mysql" // mysql
)

func NewDBPool(connString string) (*dbutil.DbHelper, error) {
	return dbutil.NewDBPool("mysql", connString)
}
